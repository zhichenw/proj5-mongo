"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import datetime
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
   """
   acp_table = [
      (200, 15, 34),
      (400, 15, 32),
      (600, 15, 30),
      (1000, 11.428, 28),
   ]
   brevet_start_time = arrow.get(brevet_start_time)
   total_time = calc_open_time(control_dist_km, brevet_dist_km, acp_table, -1)
   (hours, minutes) = timeToHM(total_time)
   return brevet_start_time.shift(hours=+hours, minutes=+minutes).isoformat()

def calc_open_time(control_dist, brevet_dist, acp_table, i):
   # base case
   if brevet_dist == 0:
      return 0
   # move to the correct acp_table row 
   if_next_row = abs(i - 1) <= len(acp_table)
   if if_next_row:
      next_dist = acp_table[i - 1][0]
   else:
      next_dist = 0
   if if_next_row and brevet_dist <= next_dist:
      return calc_open_time(control_dist, brevet_dist, acp_table, i - 1)
   # calc time
   max_speed = acp_table[i][2]
   if control_dist >= brevet_dist:
      total_time = (brevet_dist - next_dist) / max_speed
      return total_time + calc_open_time(next_dist, next_dist, acp_table, i - 1)
   if control_dist <= next_dist:
      return calc_open_time(control_dist, next_dist, acp_table, i - 1)
   else:
      total_time = (control_dist - next_dist) / max_speed
      return total_time + calc_open_time(next_dist, next_dist, acp_table, i - 1)

def timeToHM(time):
   hours = math.floor(time)
   minutes = round((time - hours) * 60)
   return (hours, minutes)

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
   """
   acp_table = [
      (200, 15, 34),
      (400, 15, 32),
      (600, 15, 30),
      (1000, 11.428, 28),
   ]
   brevet_start_time = arrow.get(brevet_start_time)
   total_time = calc_close_time(control_dist_km, brevet_dist_km, acp_table, -1)
   hours, minutes = timeToHM(total_time)
   # special cases
   if brevet_dist_km == 200 and control_dist_km >= 200:
      minutes += 10
   if brevet_dist_km == 400 and control_dist_km >= 400:
      minutes += 20
   return brevet_start_time.shift(hours=+hours, minutes=+minutes).isoformat()

def calc_close_time(control_dist, brevet_dist, acp_table, i):
   # base case
   if brevet_dist == 0:
      return 0
   # move to the correct acp_table row 
   if_next_row = abs(i - 1) <= len(acp_table)
   if if_next_row:
      next_dist = acp_table[i - 1][0]
   else:
      next_dist = 0
   if if_next_row and brevet_dist <= next_dist:
      return calc_close_time(control_dist, brevet_dist, acp_table, i - 1)
   # calc time
   min_speed = acp_table[i][1]
   if control_dist >= brevet_dist:
      total_time = (brevet_dist - next_dist) / min_speed
      return total_time + calc_close_time(next_dist, next_dist, acp_table, i - 1)
   if control_dist <= next_dist:
      return calc_close_time(control_dist, next_dist, acp_table, i - 1)
   else:
      total_time = (control_dist - next_dist) / min_speed
      return total_time + calc_close_time(next_dist, next_dist, acp_table, i - 1)