import os
from flask import Flask, redirect, url_for, request, render_template, jsonify
from pymongo import MongoClient
import arrow
import acp_times

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.acp_time

@app.route("/")
@app.route("/index")
def index():
    return render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type=int)
    start_time = request.args.get('start_time')
    open_time = acp_times.open_time(km, distance, arrow.get(start_time).isoformat())
    close_time = acp_times.close_time(km, distance, arrow.get(start_time).isoformat())
    result = {"open": open_time, "close": close_time}

    return jsonify(result=result)

@app.route('/control_times')
def control_times():
    n = db.control_times.find().count()
    if n == 0:
        return render_template('control_times.html', control_times=None)
    else:
        control_times = db.control_times.find().limit(1)[0]
    
    return render_template('control_times.html', control_times=control_times)

@app.route('/submit_control_times', methods=['POST'])
def submit_control_times():
    control_times = request.json
    if len(control_times["controls"]) != 0:
        db.control_times.drop()
        db.control_times.insert_one(control_times)
    else:
        result = {"status": "No Control Time!"}
        return jsonify(result=result), 204

    result = {"status": "success"}

    return jsonify(result=result)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
