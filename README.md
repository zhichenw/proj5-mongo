# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

Name: Zhi Chen Wen

Email: zhichenw@uoregon.edu

## ACP rules

* Maximum speed is used for calculating opening time and minimum speed is used for calculating closing time. In some cases, for example, when the control distance is 60KM, and the brevet is 200KM, opening and closing time is simply control_distance/max_speed and control_distance/min_speed. However, if the brevet is larger than 200KM, for instance, 600KM brevet and 500KM control, we need to calculate the opening and closing time using different max and min speeds (Opening time is 200/34 + 200/32 + 100/30 = 15H28M and closing time is 200/15 + 200/15 + 100/15 = 33H20M).

* If the control distance is larger than the brevet distance, we calculate opening time and closing time by brevet_distance/max_speed and brevet_distance/min_speed.

* Closing time are set for some distances. These special cases are 13H30M for 200 KM (instead of 200/15 = 13H20M), 20H00M for 300 KM, 27H00M for 400 KM (instead of 400/15 = 26H40M), 40H00M for 600 KM, and 75H00M for 1000 KM.